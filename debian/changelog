iaxmodem (1.2.0~dfsg-3) unstable; urgency=low

  * Fix build rules (Closes: #888601)
  * Added myself to uploaders
  * update vcs fields
  * Bumped standards: initscript, rules file
  * recompile fixes bug (Closes: #857906)

 -- Joerg Dorchain <joerg@dorchain.net>  Thu, 1 Mar 2018 10:28:36 +0100

iaxmodem (1.2.0~dfsg-2) unstable; urgency=low

  * Allow building with libtiff5 (Closes: #736014).
  * jblache is no longer an uploader (Closes: #688548).
  * Switch to dpkg V3 format.
  * Fix link command for building with ld --as-needed (Closes: #641729).
  * Update VCS fields.
  * Convert to dh.
  * Explicit [L]GPL-2 in debian/copyright.
  * uild_flags.patch: pass flags from dpkg-buildflags.

 -- Tzafrir Cohen <tzafrir@debian.org>  Sat, 26 Jul 2014 22:25:16 +0300

iaxmodem (1.2.0~dfsg-1) unstable; urgency=low

  * New upstream release.

  * debian/rules:
    + Check that lib/spandsp/src/spandsp/mmx.h does not exist prior to build;
      just in case the header would reappear in the future.

  * debian/patches/01_seteuid_setegid_ordering.dpatch:
    + Removed; merged upstream.
  * debian/patches/11_build_configure-stamp.dpatch:
    + Updated; do not link unneeded libraries.
  * debian/patches/10_replacement_spandsp_mmx_h.dpatch:
    + Removed; not needed as the header is not shipped anymore and is not
      needed for a successful build.

 -- Julien BLACHE <jblache@debian.org>  Sun, 22 Feb 2009 11:56:35 +0100

iaxmodem (1.1.1~dfsg-3) unstable; urgency=low

  * debian/iaxmodem.init:
    + Added status action (closes: #498219).
  * debian/watch:
    + Changed URL to use Debian QA redirector for SourceForge.

 -- Julien BLACHE <jblache@debian.org>  Sun, 15 Feb 2009 18:10:00 +0100

iaxmodem (1.1.1~dfsg-2) unstable; urgency=low

  * debian/patches/01_seteuid_setegid_ordering.dpatch:
    + Added; call setegid() before seteuid() when dropping privileges, otherwise
      setegid() will fail once we're no longer root.

 -- Julien BLACHE <jblache@debian.org>  Wed, 30 Jul 2008 19:43:58 +0200

iaxmodem (1.1.1~dfsg-1) unstable; urgency=low

  * New upstream release.

  * debian/control:
    + Bump Standards-Version to 3.8.0 (no changes).

 -- Julien BLACHE <jblache@debian.org>  Sat, 19 Jul 2008 21:32:25 +0200

iaxmodem (1.1.0~dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Tue, 12 Feb 2008 15:29:42 +0100

iaxmodem (1.0.0~dfsg-2) unstable; urgency=low

  * debian/iaxmodem.postrm:
    + Remove /var/log/iaxmodem on purge.

 -- Julien BLACHE <jblache@debian.org>  Mon, 31 Dec 2007 18:44:13 +0100

iaxmodem (1.0.0~dfsg-1) unstable; urgency=low

  * New upstream release.
    + Just a renamed 0.3.2 as far as Linux is concerned.
  * debian/control:
    + Bump Standards-Version to 3.7.3 (no changes).

 -- Julien BLACHE <jblache@debian.org>  Fri, 07 Dec 2007 18:41:23 +0100

iaxmodem (0.3.2~dfsg-2) unstable; urgency=low

  * debian/rules:
    + Fix parallel builds.

 -- Julien BLACHE <jblache@debian.org>  Sun, 02 Dec 2007 12:26:21 +0100

iaxmodem (0.3.2~dfsg-1) unstable; urgency=low

  [ Julien BLACHE ]
  * debian/copyright:
    + Updated; New files appeared in compat/.

  [ Faidon Liambotis ]
  * New upstream release.
  * debian/patches/11_build_configure-stamp.dpatch:
    + Updated.

 -- Julien BLACHE <jblache@debian.org>  Fri, 12 Oct 2007 18:42:10 +0200

iaxmodem (0.3.1~dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Sun, 05 Aug 2007 10:58:34 +0200

iaxmodem (0.3.0~dfsg-2) unstable; urgency=low

  * debian/rules:
    + Do not ignore make distclean errors.
  * debian/compat, debian/control:
    + Upgrade to DH_COMPAT 5.

 -- Julien BLACHE <jblache@debian.org>  Thu, 02 Aug 2007 18:33:04 +0200

iaxmodem (0.3.0~dfsg-1) unstable; urgency=low

  * New upstream release.
    + Now supports V.17 reception; see changelog for details.

 -- Julien BLACHE <jblache@debian.org>  Tue, 15 May 2007 09:43:53 +0200

iaxmodem (0.2.1~dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Sat, 24 Feb 2007 12:38:11 +0100

iaxmodem (0.2.0~dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Wed,  3 Jan 2007 17:03:02 +0100

iaxmodem (0.1.16~dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Thu, 23 Nov 2006 09:20:43 +0100

iaxmodem (0.1.15~dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/rules:
    + Use new ~dfsg versionning scheme.
    + Do not remove config.* in get-orig-source.
  * debian/patches/11_build_configure-stamp.dpatch:
    + Updated.
  * debian/iaxmodem.init:
    + Added LSB header.

 -- Julien BLACHE <jblache@debian.org>  Sat, 28 Oct 2006 10:54:55 +0200

iaxmodem (0.1.14.dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Tue,  1 Aug 2006 09:07:22 +0200

iaxmodem (0.1.13.dfsg-1) unstable; urgency=low

  * New upstream release.
    + Fixes iaxmodem always reporting a busy state under some circumstances.

 -- Julien BLACHE <jblache@debian.org>  Tue, 25 Jul 2006 09:09:13 +0200

iaxmodem (0.1.12.dfsg-1) unstable; urgency=low

  * New upstream release.
    + Fixes iaxmodem lock-up under some circumstances when sending faxes.
  * debian/patches/01_manpage_fixes.dpatch:
    + Removed; merged upstream.

 -- Julien BLACHE <jblache@debian.org>  Sun, 23 Jul 2006 21:51:37 +0200

iaxmodem (0.1.10.dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/11_build_configure-stamp.dpatch:
    + Updated.
  * debian/patches/01_manpage_fixes.dpatch:
    + Added; fix path and name of the configuration files in the manpage
      (closes: #377037).

 -- Julien BLACHE <jblache@debian.org>  Sat,  8 Jul 2006 12:26:18 +0200

iaxmodem (0.1.9.dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/patches/11_iax2_CORE-2006-0327.dpatch:
    + Removed; merged upstream.
  * debian/patches/11_build_configure-stamp.dpatch:
    + Added; do not build libraries if configure-stamp exists (debian/rules
      takes care of that).

 -- Julien BLACHE <jblache@debian.org>  Mon, 12 Jun 2006 17:49:03 +0200

iaxmodem (0.1.8.dfsg-2) unstable; urgency=low

  * debian/patches/11_iax2_CORE-2006-0327.dpatch:
    + Added; Fix IAXclient truncated frames vulnerabilities; see CORE-2006-0327
      (stollen from iaxclient).

 -- Julien BLACHE <jblache@debian.org>  Mon, 12 Jun 2006 10:52:16 +0200

iaxmodem (0.1.8.dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Sat,  3 Jun 2006 16:03:35 +0200

iaxmodem (0.1.7.dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Thu, 25 May 2006 20:54:17 +0200

iaxmodem (0.1.6.dfsg-1) unstable; urgency=low

  * New upstream release.

 -- Julien BLACHE <jblache@debian.org>  Thu, 25 May 2006 10:06:20 +0200

iaxmodem (0.1.5.dfsg-1) unstable; urgency=low

  * New upstream release.
    + Fix typo in x86-64 asm code (closes: #366076).
  * debian/control:
    + Bumped Standards-Version to 3.7.2 (no changes).

 -- Julien BLACHE <jblache@debian.org>  Sat,  6 May 2006 12:45:24 +0200

iaxmodem (0.1.4.dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Bumped Standards-Version to 3.7.0 (no changes).

 -- Julien BLACHE <jblache@debian.org>  Wed,  3 May 2006 08:33:42 +0200

iaxmodem (0.1.3.dfsg-1) unstable; urgency=low

  [ Julien BLACHE ]
  * New upstream release.
  * debian/rules:
    + Remove CVS dirs from the tarball (libiax2).
  * debian/iaxmodem.logrotate:
    + Use olddir to avoid rotating already rotated logfiles.

  [ Kilian Krause ]
  * debian/rules:
    + Add get-orig-source target with proper DFSG-repacking.

 -- Julien BLACHE <jblache@debian.org>  Tue, 11 Apr 2006 13:09:24 +0200

iaxmodem (0.1.2.dfsg-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    + Set Maintainer: to Debian VoIP Team, add myself as uploader.
  * debian/rules:
    + Use dpatch.
    + Detect non-Debian sources.
  * debian/patches/10_replacement_spandsp_mmx_h.dpatch:
    + Use a replacement spandsp/mmx.h header.

 -- Julien BLACHE <jblache@debian.org>  Tue, 21 Mar 2006 15:29:58 +0100

iaxmodem (0.1.1-2) unstable; urgency=low

  * Source package cleanup
    + Remove lib/spandsp/src/msvc
    + Replace lib/spandsp/src/spandsp/mmx.h with a dummy file,
      disabling MMX and SSE2.
  * debian/copyright:
    + List GPL and LGPL files for libiax2.

 -- Julien BLACHE <jblache@debian.org>  Fri, 17 Mar 2006 15:21:44 +0100

iaxmodem (0.1.1-1) unstable; urgency=low

  * New upstream release.
    + iaxmodem now waits for Asterisk (integrates previous patch).
  * build:
    + build with -std=c99 -D_GNU_SOURCE to fix the lrintf() warning, which
      is a real problem on 64bit architectures.

 -- Julien BLACHE <jblache@debian.org>  Thu, 16 Mar 2006 13:39:05 +0100

iaxmodem (0.1.0-3) unstable; urgency=low

  * iaxmodem.c:
    + Do not exit if modem is free and we can't register. This allows us to
      for Asterisk to come up at startup (and allows for restarts of Asterisk).
  * debian/rules:
    + Start before HylaFax and Asterisk, stop after HylaFax but before Asterisk.
  * debian/README.Debian:
    + Changing HylaFax initscripts' priorities is no longer needed.

 -- Julien BLACHE <jblache@debian.org>  Tue, 14 Mar 2006 15:13:51 +0100

iaxmodem (0.1.0-2) unstable; urgency=low

  * debian/rules:
    + The init script must run after asterisk.
  * debian/README.Debian:
    + Instructions to make the HylaFax init scripts run after iaxmodem.

 -- Julien BLACHE <jblache@debian.org>  Sun, 12 Mar 2006 21:00:01 +0100

iaxmodem (0.1.0-1) unstable; urgency=low

  * Initial Release (closes: #354560).

  This package has been contributed to Debian by Linbox (http://www.linbox.com).

 -- Julien BLACHE <jblache@debian.org>  Thu,  2 Mar 2006 18:05:16 +0100

